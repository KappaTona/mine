#pragma once

#include <QWidget>
#include <QGridLayout>
#include <vector>
#include <utility>
#include <QPointer>

class QDialog;
class field_button;
class mine_board : public QWidget
{
	Q_OBJECT
	int mine_field_[8][8];
	void game_over();
	QPointer<QGridLayout> game_;
	void victory_or_not();
	int bomb_counter_ = 0;
	void print_minefield() const;
	bool victory_ = false;
	bool check_every_cell();
public:
	mine_board(QDialog* diag);
	void emit_reset();
public Q_SLOT:
	void maybe_victory();
};
