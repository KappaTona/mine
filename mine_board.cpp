#include "mine_board.h"
#include "field_button.h"

#include <QDialog>
#include <QPushButton>
#include <QCoreApplication>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLayoutItem>
#include <QPointer>

#include <time.h>
#include <stdlib.h>
#include <iostream>

mine_board::mine_board(QDialog* diag)
{
	for (int i = 0; i < 8; ++i)
		for (int j = 0; j < 8; ++j)
			mine_field_[i][j] = 0;

	auto* exit_btn = new QPushButton("exit");
	connect(exit_btn, &QPushButton::clicked, qApp, &QCoreApplication::quit);
	exit_btn->show();

	auto* first_row = new QVBoxLayout;
	first_row->addWidget(exit_btn);

	game_ = new QGridLayout;
	game_->addLayout(first_row, 0, 1);

	bomb_counter_ = 0;
	srand(time(NULL));
	for (int i = 0; i < 36; ++i)
	{
		bool boom = false;
		int row = i / 6 + 1;
		int col = i % 6;
		if ( !(rand() % 3) && bomb_counter_ < 10 )
		{
			++bomb_counter_;
			boom = true;
		}
//		auto name = QString::number(i);
		mine_field_[row][col+1] = boom ? 1 : 0;
		QPointer<field_button> btn{new field_button(boom, row, col, this)};
		game_->addWidget(btn, row, col);

		connect(btn, &field_button::right_click, [this](){
				maybe_victory();
			});
		connect(btn, &field_button::info, [this](field_button* b)
			{ 
				if (b->is_bomb())
				{
					b->setText("BOOOOM");
					game_over();
				}
				else 
				{
					int row, col;
					std::tie(row, col) = b->pos();

					int counter = 
						mine_field_[row - 1][col] + 
						mine_field_[row - 1][col+1] +
						mine_field_[row - 1][col + 2] +
						mine_field_[row][col] +
						mine_field_[row][col + 2] +
						mine_field_[row + 1][col] + 
						mine_field_[row + 1][col+1] +
						mine_field_[row + 1][col + 2];

					b->setText(QString::number(counter));
				}
			});
	}

	diag->setLayout(game_);
	update();
}

bool mine_board::check_every_cell()
{
	int counter = 0;
	for (int i = 0; i < game_->count(); ++i)
	{
		auto* l = game_->itemAt(i);
		auto* lwd = l->widget();
		auto* btn = qobject_cast<field_button*>(lwd);
		if (btn) 
		{
			if (btn->is_marked())
				++counter;
		}
	}
	victory_ = counter == bomb_counter_;
	return victory_;
}

void mine_board::game_over()
{
	for (int i = 0; i < game_->count(); ++i)
	{
		auto* l = game_->itemAt(i);
		auto* lwd = l->widget();
		auto* btn = qobject_cast<field_button*>(lwd);
		if (btn) 
		{
			btn->done();
		}
	}

	victory_or_not();
}
	
void mine_board::victory_or_not()
{
	QPointer<QDialog> diag = new QDialog;
	auto* hbox = new QHBoxLayout;
	auto* btn = new QPushButton{victory_ ? "VICTORY" : "DEFEAT"};
	hbox->addWidget(btn);
	diag->setLayout(hbox);
	diag->show();

	connect(btn, &QPushButton::clicked, [=](){
			qApp->exit(1000);
		});

}

void mine_board::print_minefield() const
{
	for (int i = 1; i < 7; ++i)
	{
		for (int j = 1; j < 7; ++j)
		{
			//std::cerr << "[" << i << "][" << j << "]=" <<
				std::cerr <<	mine_field_[i][j] << " ";
		}
		std::cerr << std::endl;
	}
}

void mine_board::maybe_victory()
{
	if (check_every_cell())
		victory_or_not();
}
