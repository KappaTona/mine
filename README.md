# What is [MineSweeper](https://en.wikipedia.org/wiki/Minesweeper_(video_game)) 

Requirements (are given as at least):
---
- 1024x768 resolution
- 6x6 table with maximum of 10 bombs (bonus the bombs always random, see below)
- if user finds a bomb the game ends and reveals all bombs
- if user has revealed all the fields and marked all the bombs the game ends.
	(Caution, if the user marked all the bombs, the game does not end)
- if user clicks on button and the button is not a bomb, then the text must change to the number of bombs
	nearby. **


## Bonus
 
*should be started after the core mechanics are done*

- The game can be restarted
- The game has random position of bombs every time it starts. tipp use modulo 3 and random generated
	values. *
- Designing the game, such as prettier buttons, sound effects
- Using git as version controller, hint use [gitlab](https://gitlab.com)
- Lightweight [git guide](https://rogerdudler.github.io/git-guide/)


## Ideas  
> class field button  
function is\_bomb();  
function is\_marked();  

> class mine\_board  
member int[][] mine\_field;  
function fill\_mine\_field();  
function check\_neighbours();  
mechanism check\_every\_cell\_for\_victory\_or\_defeat();  

---
## Foot notes:
  
### * little tip for random number gens in C++

	int bomb_counter;  
	for (int i = 0; i < 36; ++i)  
	{
		bool boom = false;
		int row = i / 6 + 1;
		int col = i % 6;
		if ( !(rand() % 3) && bomb_counter < 10 )
		{
			++bomb_counter;
			boom = true;
		}	
	}  
  
  

### ** sample:  


Let b be as bomb;  
Let c be as clear;  
Let x be as the clicked button  
  
b c c  
c x c  
c b c  
  
In this case x should be changed to the value of 2 as of the bombs number.
WARNING!! The corner cases might be problemtaic, consider using 
a bigger matrix for logic than UI (ask the task giver for further info)
