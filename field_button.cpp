#include "field_button.h"
#include <QMouseEvent>
#include <QPalette>
#include <QPoint>

field_button::field_button(bool is_bomb, 
		int row,
		int col,
		QWidget* parent)
	: QPushButton{parent}
	, is_bomb_{is_bomb}
	, row_{row}
	, col_{col}
{
	setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
	setFocusPolicy(Qt::NoFocus);

	// set black background
}

void field_button::done()
{
	if (is_bomb_ && !marked_)
		setText("BOOOOM");
	setEnabled(false);
}

void field_button::emit_is_bomb()
{
	Q_EMIT info(this);
}

void field_button::emit_right_click()
{
	Q_EMIT right_click();
}

void field_button::mousePressEvent(QMouseEvent* ev) 
{
	if (ev->buttons() == Qt::LeftButton)
	{
		emit_is_bomb();
	}

	if (ev->buttons() == Qt::RightButton)
	{
		marked_ = !marked_;
		setText(marked_? "MARKED" : "");
		emit_right_click();
	}

}

std::pair<int, int> field_button::pos() const
{
	return std::make_pair(row_, col_);
}

bool field_button::is_bomb() const
{
	return is_bomb_;
}

bool field_button::is_marked() const
{
	return marked_;
}
