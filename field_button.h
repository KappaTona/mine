#pragma once
#include <QPushButton>
#include <utility>
#include <QObject>

class QString;
class field_button : public QPushButton
{
	Q_OBJECT
public:
	field_button(bool is_bomb, 
		int row,
		int col,
		QWidget* parent = nullptr);

	bool is_bomb() const;
	bool is_marked() const;
	std::pair<int,int> pos() const;
	void done();
	void emit_right_click();
public Q_SLOTS:
	void emit_is_bomb();
protected:
	void mousePressEvent(QMouseEvent* ev) override;
private:
	bool marked_{false};
	bool is_bomb_;
	QPoint pos_;
	int row_;
	int col_;
Q_SIGNALS:
	void info(field_button*);
	void right_click();
};
