#include "setup.h"

#include <QCoreApplication>
#include <QDebug>

#include <iostream>

setup::setup(int w, int h)
{
	view_ = new QDialog;
	view_->setFixedWidth(w);
	view_->setFixedHeight(h);
	view_->show();
}

void setup::clean_up()
{
	delete view_;
}

QDialog* setup::main_view() const
{
	return view_;
}

//setup::~setup()
//{
//	std::cout << "setup::~setup() called" << std::endl;
//	clean_up();
//}
